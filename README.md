# Input Method Playground

This repo contains some non-convnentional usages of InputMethods to explore new ways of improving text input.

# Setting up

Install as usual:

```
mkdir build
cd build
cmake ..
make
make install
```

Under System Settings -> Virtual Keyboards there will now be a new entry "Input playground".

# Usage

See the code for the hardcoded shortcuts.

# Contributing

For development, I strongly suggest disabling kwin's security model and then just launching as a standalone binary,
Feel free to push any code, it's meant as a playground so we can merge whatever. 

I don't expect to release from here, we'll cherry-pick out what works well into Plasma as new MRs in a feature ready state. I don't imagine us merging anything into Plasma before InputMethodV3 is standardised.
