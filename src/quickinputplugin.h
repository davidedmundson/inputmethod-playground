#pragma once

#include <QObject>
#include <QUrl>

#include "inputplugin.h"

class QQuickItem;
namespace PlasmaQuick {
class PlasmaWindow;
class SharedQmlEngine;
}


/**
 * @brief The QuickInputPlugin class extends the InputPlugin
 * with a path to show a QtQuickView as an InputPanel
 *
 * The plugin itself will be registered as a context property
 * of any loaded QML with the contextProperty "InputPlugin"
 *
 * This allows communication to the plugin
 */
class QuickInputPlugin : public InputPlugin
{
    Q_OBJECT

    // I had issues using Window.window.visible, if someone can fix that, use that instead
    Q_PROPERTY(bool windowVisible READ windowVisible WRITE setWindowVisibility NOTIFY windowVisibilityChanged)
public:
    QuickInputPlugin(InputMethod *inputMethod);
    ~QuickInputPlugin();
    void setSource(const QUrl &url);
    void showWindow();
    void hideWindow();

    bool windowVisible() const;
    void setWindowVisibility(bool visible);

    void keyPressed(QKeyEvent *keyEvent) override;
    void keyReleased(QKeyEvent *keyEvent) override;

Q_SIGNALS:
    void windowVisibilityChanged();

private:
    QQuickItem *m_item = nullptr;
    PlasmaQuick::PlasmaWindow *m_window;
    PlasmaQuick::SharedQmlEngine *m_engine;

};
