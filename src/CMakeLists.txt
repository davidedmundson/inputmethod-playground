ecm_add_qtwayland_client_protocol(SRCS
    PROTOCOL ${WaylandProtocols_DATADIR}/unstable/input-method/input-method-unstable-v1.xml
    BASENAME input-method-unstable-v1
)
ecm_add_qtwayland_client_protocol(SRCS
    PROTOCOL ${WaylandProtocols_DATADIR}/unstable/text-input/text-input-unstable-v1.xml
    BASENAME text-input-unstable-v1
)
ecm_add_qtwayland_client_protocol(SRCS
    PROTOCOL "${Wayland_DATADIR}/wayland.xml"
    BASENAME wayland
)

add_executable(input-method-playground
    #lib parts
    main.cpp
    inputmethod.cpp

    inputplugin.cpp
    quickinputplugin.cpp
    qwaylandinputpanelshellintegration.cpp
    qwaylandinputpanelsurface.cpp
    ${SRCS}

#plugins
    style/style.cpp

    clipboard/clipboardintegrator.cpp
    clipboard/resources.qrc

    emojis/emojiintegrator.cpp
    emojis/resources.qrc

    diacritics/diacritics.cpp
    diacritics/resources.qrc

    translate/translate.cpp
    translate/resources.qrc
)

target_link_libraries(input-method-playground
    Qt6::Quick
    Qt6::GuiPrivate
    Qt::WaylandClientPrivate
    Qt6::WaylandClient
    Wayland::Client
    Plasma::PlasmaQuick
    KF6::GuiAddons
)

install(TARGETS input-method-playground DESTINATION bin)
install(FILES org.davidedmundson.kdeinputplaygound.desktop DESTINATION share/applications)
