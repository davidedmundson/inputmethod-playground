#include "translate.h"

#include <KSystemClipboard>
#include <QMimeData>

Translate::Translate(InputMethod *inputMethod)
    : QuickInputPlugin(inputMethod)
{
    setSource(QUrl("qrc:/translate/main.qml"));
    setGrabbing(true);

    m_translateProcess.setProgram("/usr/bin/trans");
    connect(&m_translateProcess, &QProcess::finished, this, [this]() {
        // TODO check exit code
        QString translation = m_translateProcess.readAllStandardOutput();

        if (m_isSubmittingCommit) {
            commit(translation);
            m_isSubmittingCommit = false;
        } else {
            setPreEditString(translation);
            setPreEditStyle(0, translation.length(), 3);
            setPreEditCursor(translation.length());
        }
    });

    connect(&m_translateProcess, &QProcess::finished, this, [this]() {
        if (!m_pendingPreview.isEmpty()) {
            translate(m_pendingPreview);
            m_pendingPreview.clear();
        }
        }, Qt::QueuedConnection);

    connect(&m_translateProcess, &QProcess::stateChanged, this, &Translate::isBusyChanged);

    connect(this, &InputPlugin::contextChanged, this, [this]() {
        m_translateProcess.kill();
        m_pendingPreview.clear();
        m_isSubmittingCommit = false;
        hideWindow();
    });
}

Translate::~Translate() {}

void Translate::keyPressed(QKeyEvent *keyEvent)
{
    if (keyEvent->key() == Qt::Key_T && keyEvent->modifiers().testFlags({Qt::MetaModifier, Qt::ShiftModifier})) {
        onShortcut();
        keyEvent->setAccepted(true);
    } else if (keyEvent->key() == Qt::Key_Escape) {
        hideWindow();
    }
    QuickInputPlugin::keyPressed(keyEvent);
}

bool Translate::isBusy()
{
    return m_translateProcess.state() == QProcess::Running;
}

void Translate::onShortcut() {
    showWindow();
}

void Translate::previewTranslation(const QString &text)
{
    if (text.isEmpty()) {
        setPreEditString(QString());
        return;
    }
    if (isBusy()) {
        m_pendingPreview = text;
        return;
    }
    translate(text);
}

void Translate::commitTranslation(const QString &text)
{
    // this isn't ideal, we could have multiple commits in progress, we maybe need to maintain a full stack of jobs?
    m_translateProcess.kill();
    m_pendingPreview.clear();
    if (text.isEmpty()) {
        return;
    }
    m_isSubmittingCommit = true;
    translate(text);
}

void Translate::translate(const QString &text)
{
    qDebug() << "translating" << text;
    QStringList args;
    //TODO obviously this hardcoded language part need to change
    args << "-b" << "en:de" << text;
    m_translateProcess.setArguments(args);
    m_translateProcess.start();
}
