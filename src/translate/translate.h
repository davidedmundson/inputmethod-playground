#include "quickinputplugin.h"

#include <QStringListModel>
#include <QProcess>
#include <QTimer>

class InputMethodContext;

class Translate : public QuickInputPlugin
{
    Q_OBJECT
    Q_PROPERTY(bool busy READ isBusy NOTIFY isBusyChanged)
    // TODO properties for source and target language
public:
    Translate(InputMethod *inputMethod);
    ~Translate();

    void keyPressed(QKeyEvent *keyEvent) override;

    bool isBusy();

    Q_INVOKABLE void previewTranslation(const QString &text);
    Q_INVOKABLE void commitTranslation(const QString &text);
Q_SIGNALS:
    void isBusyChanged();
private:
    void onShortcut();
    void translate(const QString &text);

    QString m_pendingPreview;
    bool m_isSubmittingCommit = false;
    QProcess m_translateProcess;
};
