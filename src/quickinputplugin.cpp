#include "quickinputplugin.h"

#include <QDebug>
#include <memory>

#include <PlasmaQuick/PlasmaWindow>
#include <PlasmaQuick/SharedQmlEngine>
#include <QQmlEngine>
#include <QQuickItem>
#include <QGuiApplication>

#include "qwaylandinputpanelshellintegration_p.h"
#include <QtWaylandClient/private/qwaylandwindow_p.h>

// should QML files should control their own visibility or the C++ backend?

QuickInputPlugin::QuickInputPlugin(InputMethod *inputMethod)
    : InputPlugin(inputMethod)
{
    m_engine = new PlasmaQuick::SharedQmlEngine(this);

    m_window = new PlasmaQuick::PlasmaWindow;
    m_window->create();
    connect(m_window, &QQuickWindow::visibilityChanged, this, &QuickInputPlugin::windowVisibilityChanged);

    auto waylandWindow = dynamic_cast<QtWaylandClient::QWaylandWindow *>(m_window->handle());
    if (!waylandWindow) {
        qWarning() << m_window << "is not a wayland window. Not creating panel";
        return;
    }
    static QWaylandInputPanelShellIntegration *shellIntegration = nullptr;
    if (!shellIntegration) {
        shellIntegration = new QWaylandInputPanelShellIntegration();
        if (!shellIntegration->initialize(waylandWindow->display())) {
            delete shellIntegration;
            shellIntegration = nullptr;
            qWarning() << "Failed to initialize input panel-shell integration, possibly because compositor does not support the layer-shell protocol";
            return;
        }
    }
    m_window->requestActivate();
    waylandWindow->setShellIntegration(shellIntegration);
}

QuickInputPlugin::~QuickInputPlugin()
{
}

void QuickInputPlugin::setSource(const QUrl &url)
{
    //TODO handle setting this more than once, or error out if it is
    m_engine->rootContext()->setContextProperty(QStringLiteral("InputPlugin"), this);
    m_engine->setSource(url);

    auto mainItem = qobject_cast<QQuickItem *>(m_engine->rootObject());
    if (!mainItem) {
        qFatal("Fix your QML");
    }
    m_window->setMainItem(mainItem);

    auto updateWindowSize = [mainItem, this]() {
        const QMargins margins = m_window->frameMargins();
        m_window->resize(QSize(mainItem->implicitWidth(), mainItem->implicitHeight()).grownBy(margins));
    };

    connect(mainItem, &QQuickItem::implicitHeightChanged, this, updateWindowSize);
    connect(mainItem, &QQuickItem::implicitWidthChanged, this, updateWindowSize);
    updateWindowSize();
}

void QuickInputPlugin::showWindow()
{
    m_window->show();
    QFocusEvent focusEvent(QEvent::FocusIn, Qt::ActiveWindowFocusReason);
    qApp->sendEvent(m_window, &focusEvent);
}

void QuickInputPlugin::hideWindow()
{
    m_window->hide();
}

bool QuickInputPlugin::windowVisible() const
{
    return m_window->isVisible();
}

void QuickInputPlugin::setWindowVisibility(bool visible)
{
    if (visible) {
        showWindow();
    } else {
        hideWindow();
    }
}

void QuickInputPlugin::keyPressed(QKeyEvent *keyEvent)
{
    if (m_window && m_window->isVisible() && !keyEvent->isAccepted()) {
        qApp->sendEvent(m_window, keyEvent);
    }
}

void QuickInputPlugin::keyReleased(QKeyEvent *keyEvent)
{
    if (m_window && m_window->isVisible() && !keyEvent->isAccepted()) {
        qApp->sendEvent(m_window, keyEvent);
    }
}
