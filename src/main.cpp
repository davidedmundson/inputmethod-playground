/*
    SPDX-FileCopyrightText: 2021 Aleix Pol Gonzalez <aleixpol@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include <QDebug>
#include <QGuiApplication>

#include "inputmethod_p.h"
#include "clipboard/clipboardintegrator.h"
#include "emojis/emojiintegrator.h"
#include "translate/translate.h"
#include "diacritics/diacritics.h"
#include "style/style.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    InputMethod inputMethod;
    ClipboardIntegrator clip(&inputMethod);
    EmojiIntegrator emojis(&inputMethod);
    Translate translate(&inputMethod);
    Diacritics diacritics(&inputMethod);
    Style style(&inputMethod);
    return app.exec();
}

#include "main.moc"
