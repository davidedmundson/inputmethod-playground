import QtQuick
import org.kde.plasma.components
import org.kde.plasma.extras as PlasmaExtras

ListView
{
    id: root
    implicitWidth: 200
    implicitHeight: Math.min(400, contentHeight)
    model: InputPlugin.entryModel
    currentIndex: InputPlugin.currentIndex

    highlight: PlasmaExtras.Highlight { }

    delegate: Label {
        text: model.display
        wrapMode: Text.NoWrap
        elide: Text.ElideRight
        width: parent.width
    }
}
