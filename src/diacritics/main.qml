import QtQuick
import QtQuick.Layouts
import QtQuick.Window

import org.kde.plasma.components

RowLayout {
    Binding {
        target: InputPlugin
        property: "windowVisible"
        value: InputPlugin.active
    }

    Repeater {
        model: InputPlugin.charactersModel
        delegate: MouseArea {
            width: 28
            height: 28
            onClicked: InputPlugin.commitTranslation(model.display)
            Label {
                anchors.centerIn: parent
                font.pointSize: 25
                text: model.display
            }
        }
    }
    // this needs navigation and selection and maybe some hot keys.
    // but it'll do for now...
}
