#include "quickinputplugin.h"

#include <QTimer>
#include <QStringListModel>


class InputMethodContext;

class Diacritics : public QuickInputPlugin
{
    Q_OBJECT
    Q_PROPERTY(QStringListModel* charactersModel READ charactersModel CONSTANT)
    Q_PROPERTY(bool active READ isActive NOTIFY activeChanged)

public:
    Diacritics(InputMethod *inputMethod);
    ~Diacritics();

    void keyPressed(QKeyEvent *keyEvent) override;
    void keyReleased(QKeyEvent *keyEvent) override;

    bool isActive();
    QStringListModel* charactersModel();

    Q_INVOKABLE void commitTranslation(const QString &text);
Q_SIGNALS:
    void activeChanged();
private:
    void onHeldTimeout();
    QString m_pressedKey = 0;
    QTimer m_isHeldTimer;
    QStringListModel *m_charactersModel;
};
