/*
    SPDX-FileCopyrightText: 2021 Aleix Pol Gonzalez <aleixpol@kde.org>

    SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
*/

#include "emojiintegrator.h"
#include <QDebug>

EmojiIntegrator::EmojiIntegrator(InputMethod *inputMethod)
    : QuickInputPlugin(inputMethod)
{
    setSource(QUrl("qrc:/emojis/main.qml"));
    connect(this, &InputPlugin::surroundingTextChanged, this, &EmojiIntegrator::refreshBracketedText);
}

EmojiIntegrator::~EmojiIntegrator()
{
}

void EmojiIntegrator::setBracketedText(const QString &text)
{
    if (m_bracketedText != text) {
        m_bracketedText = text;
        Q_EMIT bracketedTextChanged(text);
    }

    if (m_bracketedText.length()) {
        setGrabbing(true);
    } else {
        setGrabbing(false);
    }
}

void EmojiIntegrator::refreshBracketedText()
{
    if (contentPurpose() != InputPlugin::content_purpose_normal) {
        setBracketedText(QString());
        return;
    }

    const QString text = surroundingText();
    const int cursor = cursorPos();

    const int index = text.left(cursor).lastIndexOf(QLatin1Char(':'));

    if (index < 0) {
        setBracketedText({});
    } else {
        setBracketedText(text.mid(index + 1, cursor - index - 1));
    }
}

void EmojiIntegrator::replaceBracket(const QString &text)
{
    const int index = surroundingText().left(cursorPos()).lastIndexOf(QLatin1Char(':'));
    if (index < 0) {
        qWarning() << "no bracket, bro!" << surroundingText();
        return;
    }

    // This is a bit of a mess, see Kwin InputMethod::deleteSurrounding
    // we're not using the args quite as described
    deleteSurroundingText(-(cursorPos() - index), cursorPos() - index);
    commit(text);
}
