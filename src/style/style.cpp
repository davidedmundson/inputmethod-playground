#include "style.h"

#include <QMimeData>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <functional>

Style::Style(InputMethod *inputMethod)
    : InputPlugin(inputMethod)
    , mgr(new QNetworkAccessManager(this))
{
    const auto key = qgetenv("OPENAI_API_KEY");
    if (key.isEmpty()) {
        qWarning() << "Cannot use the style plugin without OPENAI_API_KEY";
    }
    setGrabbing(!key.isEmpty());
}

Style::~Style() {}

void Style::keyPressed(QKeyEvent *keyEvent)
{
    if (keyEvent->key() == Qt::Key_S && keyEvent->modifiers().testFlags({Qt::MetaModifier, Qt::ShiftModifier})) {
        onShortcut();
        keyEvent->setAccepted(true);
    }
    InputPlugin::keyPressed(keyEvent);
}

void Style::query(const QString &inputRequest, const QString &input, std::function<void(QString)> response)
{
    if (input.isEmpty()) {
        response("🌸");
        return;
    }
    // response("\n\nPlease find attached the invoice for the 2023 membership fee, totaling AMOUNT. We are happy to address any questions or concerns you may have about this payment. For further details, please refer to the information provided here:");
    // return;

    QNetworkRequest request(QUrl(QStringLiteral("https://api.openai.com/v1/chat/completions")));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("Authorization", "Bearer " + qgetenv("OPENAI_API_KEY"));

    const QJsonObject obj = {
        {"model", "gpt-3.5-turbo"},
        {"messages", QJsonArray {
            QJsonObject {
                {"role", "system"},
                {"content", inputRequest}
            },
            QJsonObject {
                {"role", "user"},
                {"content", input }
            }
        }}
    };

    QByteArray data = QJsonDocument(obj).toJson();
    m_currentReply = mgr->post(request, data);
    // QTextStream(stdout) << "curl " << request.url().toString() << " -H \"Authorization: Bearer $OPENAI_API_KEY\" -H \"Content-Type: application/json\" -d '" << QJsonDocument(obj).toJson(QJsonDocument::Compact) << "'" << Qt::endl;

    connect(m_currentReply, &QNetworkReply::errorOccurred, [](QNetworkReply::NetworkError e){
        qDebug() << "error" << e;
    });
    connect(m_currentReply, &QNetworkReply::finished, [this, response](){
        if(m_currentReply->error() == QNetworkReply::NoError){
            const auto contents = QJsonDocument::fromJson(m_currentReply->readAll()).object();
            qDebug() << "received reply" << contents;
            const QString content = contents["choices"].toArray()[0].toObject()["message"].toObject()["content"].toString();
            response(content);
        }
        else{
            qDebug() << "Error!" << m_currentReply->errorString() << QJsonDocument::fromJson(m_currentReply->readAll());
        }
        m_currentReply->deleteLater();
        m_currentReply = nullptr;
    });
}

std::pair<uint, uint> Style::selection() const
{
    int beginning = cursorPos(), ending = anchorPos();
    if (beginning > ending)
        std::swap(beginning, ending);
    return {beginning, ending - beginning};
}

void Style::onShortcut()
{
    const auto anchors = selection();
    QString textToStyle = surroundingText().mid(anchors.first, anchors.second);
    std::function<void(const QString &)> f = [this] (const QString &output) {
        commit(output.trimmed());
    };

    if (textToStyle.isEmpty() && !surroundingText().isEmpty()) {
        // If there's no selection but surrounding text, apply it to all the text
        textToStyle = surroundingText();
        f = [this, textToStyle] (const QString &output) {
            deleteSurroundingText(-cursorPos(), textToStyle.size());
            commit(output.trimmed());
        };
    }

    qDebug() << "improving..." << textToStyle;
    if (textToStyle.isEmpty()) {
        qWarning() << "gotta gimme something br0";
        return;
    }
    query(QStringLiteral("Improve the following text:"), textToStyle, f);
}
