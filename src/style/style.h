#include "quickinputplugin.h"

#include <QStringListModel>
#include <QProcess>
#include <QTimer>

class QNetworkAccessManager;
class QNetworkReply;

class Style : public InputPlugin
{
    Q_OBJECT
public:
    Style(InputMethod *inputMethod);
    ~Style();

    void keyPressed(QKeyEvent *keyEvent) override;

private:
    std::pair<uint, uint> selection() const;

    void query(const QString &inputRequest, const QString &input, std::function<void(QString)> response);
    void onShortcut();

    QNetworkAccessManager *const mgr;
    QNetworkReply* m_currentReply;
};
